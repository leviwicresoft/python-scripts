import requests
import base64
import csv

def sendReq(uri, method="get", body=""):
    
    pat=".....av7nyomf5kxmqalzp2....."

    auth=str(base64.b64encode(bytes(":"+pat ,"ascii")),"ascii")
    headers ={
        'Accept': 'application/json'
    }
    if method=="post" :
       return requests.post(url=uri, headers={'Content-Type': 'application/json'}, auth=('', pat), json=body)
    elif method =="patch":
       return requests.patch(url=uri, headers={'Content-Type': 'application/json-patch+json'}, auth=('', pat), json=body)
    else:
       return requests.get(url=uri, auth=('', pat))

def writeTocsv(header,data):
   path="c:\\test\\pythonscripts\workitem.csv"
   
   with open(path, 'w+', newline='') as f:
      writer = csv.DictWriter(f, fieldnames=header)
      writer.writeheader()
      writer.writerows(data)
      
      
def exportwit():
    #uri = r'https://dev.azure.com/{org}/AboutSite/_apis/build/builds?definitions=87&$top=1&QueryOrder=queueTimeDescending&api-version=6.1-preview.6'
    quri = "https://dev.azure.com/{org}/_apis/wit/wiql?api-version=6.0"
    body = {
       "query": "Select [System.Id] From WorkItems Where [System.WorkItemType] = 'Task' AND EVER [System.State] = 'Closed' order by [Microsoft.VSTS.Common.Priority] asc, [System.CreatedDate] desc"
     }

    res = sendReq(quri, "post", body).json()
    #print(res)
    ids = [(x["id"], x["url"]) for x in res["workItems"]]
    #ids=[("9","https://dev.azure.com/LeviWicresoft/_apis/wit/workItems/9")]
    
    result=[]
    
    for id in ids:
        i, u= id
        res = sendReq(u+"/updates?api-version=6.0").json()
        x = res["value"][-2]
        reports = list(filter(lambda x: (x.get("fields",{"a":"b"}).get("System.State",{"a":"b"}).get("newValue",None)=="Active" and x.get("fields",{"a":"b"}).get("System.State",{"a":"b"}).get("oldValue",None)=="Closed"),  res["value"]))  
        #print(reports)
        if reports:
          d = {i:  [(x["revisedBy"]["uniqueName"],x["revisedDate"] ) for x in reports]}
          data = ""
          for element in [(x["revisedBy"]["uniqueName"],x["revisedDate"] ) for x in reports]:
             data = data + element[0] +":" + element[1] + ";\n"

          item = {"WorkItem_ID": i , "Changed_By": data, "Total_Times": len(reports)}
          result.append(item)
           
    header = ["WorkItem_ID","Changed_By", "Total_Times"]
    print(result)
    writeTocsv(header, result)
    
 

def test():
   url = "https://dev.azure.com/{org}/AboutSite/_apis/build/builds/12056/artifacts?api-version=6.1-preview.5"
   
   re = sendReq(url).json()

   ar = re["value"][0]

   ar["resource"]["type"] = "Container"

   print(ar)

   curl = "https://dev.azure.com/{org}/AboutSite/_apis/build/builds/11096/artifacts?api-version=6.1-preview.5"
   ps = sendReq(curl,"post", ar)
   return ps.json()
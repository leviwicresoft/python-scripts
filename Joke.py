import re
from urllib import request, error


class Joke:
    def __init__(self):
        self.list=[]
        
        
    def getContent(self,page):
        if page==1:
            self.loadPage(page)
        else:
            if len(self.list)<2:
                url = 'https://www.qiushibaike.com/hot/page/' +str(page)
                self.loadPage(page)
               
                
    def loadPage(self,page):
        
        url = 'https://www.qiushibaike.com/hot/page/'+ str(page)
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0"}
        try:
            req = request.Request(url,headers= headers)
            data = request.urlopen(req)
            content = data.read().decode('utf-8')
        except error.URLError as e:
            print(e.code)
        pattern= r'<h2>(.*?)</h2>.*?<div class="content">\n*<span>\n*(.*?)\n*</span>.*?<i class="number">(\d*)</i>\s*好笑'   
        items = re.findall(pattern, content,re.S)
        self.list.append(items)
        
    
    def getJoke(self,page):
        
        self.getContent(page)
        
        while len(self.list):
            items =  self.list[0]
            for item in items:
                inputkey = input()
                print('第',page,'页')
                print(*[i.replace('<br/>',' ') for i in item],sep ='\n')
                if inputkey == 'q':
                    break
            self.list.remove(items)
            self.getJoke(page+1)
            
        

    def start(self):
        
        self.getJoke(1)
        
             
joke = Joke()
joke.start()
        

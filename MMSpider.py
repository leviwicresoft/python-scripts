import urllib.request as urllib
import re
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class Spider:
    def __init__(self):

        self.url="https://mm.taobao.com/json/request_top_list.htm?page="
    def getPage(self, number):
        pageUrl = [ self.url+str(x) for x in range(1,number+1)]
        return pageUrl

    def getMM(self,pageurl):
        request =  urllib.Request(pageurl)
        response=urllib.urlopen(request)
        data = re.sub('[\s\n]+',' ',response.read().decode("gbk"))
        
        nameageplace = r'<a class="lady-name" href=.*?>(.+?)<\/a> <em><strong>(\d+)<\/strong>.+?<span>(.+?)<\/span>.+?<a href="(.+?)" (?=target=.+)'
        items = re.findall(nameageplace,data,re.S)
        with open('d:\pycode\mm.txt', 'w',encoding="utf-8") as f:
            f.write(data)
        #print(data)
        #print (type(data))
        #[print(x) for x in items]
        return items
    def getMMPic(self, items):
            driver = webdriver.Firefox()
            for item in items:
                driver.get(item[3])
                elem= driver.find_element_by_css_selector('ul.mm-p-menu span a')
                elem.click()
                wait = WebDriverWait(driver,10)
                img = wait.until(EC.presence_of_element_located(By.CSS_SELECTOR,"img"))
                imgurl=[x.get_attribute('src') for x in img]
                print(imgurl)
    def start(self):
        pageurl = self.getPage(1)
        for page in pageurl:
            self.getMMPic(self.getMM(pageurl))




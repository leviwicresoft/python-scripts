import urllib.request
import os
import random

def get_data(urls):
	request = urllib.request.Request(urls)
	request.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1; rv:51.0) Gecko/20100101 Firefox/51.0')
	response = urllib.request.urlopen(request)
	data = response.read()
	return data

def get_url(pages):
	html = get_data('http://jandan.net/ooxx').decode('utf-8')
	a= html.find('current-comment-page') +23
	page=int(html[a:a+4])
	url =[]
	for i in range(pages):
		b=page - i
		link = 'http://jandan.net/ooxx/' + 'page-' + str(b)+'#comments'
		url.append(link)
	return url

def get_img(pages):
	img_url = []
	url = get_url(pages)
	for i in url:
		img_html=get_data(i).decode('utf-8')
		a= img_html.find('img src=')
		while not a==-1:
			b = img_html.find('.jpg',a,a+255)
			if not b==-1:
				src ='http:' + img_html[a+9:b+4]
				img_url.append(src)
				a = img_html.find('img src=',b)
			else:
				a = img_html.find('img src=',a+8)
	return img_url

def save_img(folder,pages):
	images = get_img(pages)
	for each in images:
		if each:
			filename = each.split('/')[-1]
			with open(filename,'wb') as f:
				data = get_data(each)
				f.write(data)


def open_folder(folder = 'xxoo', pages = 10):
	os.chdir('c:\\code')
	try:
		os.mkdir(folder)
	except:
		pass
	finally:
		os.chdir(folder)
	
	save_img(folder,pages)


if __name__ == '__main__':
	open_folder()
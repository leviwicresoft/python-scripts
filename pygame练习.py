import pygame
import sys
from pygame.locals import *
pygame.init()
clock =  pygame.time.Clock()
size = width, height = 600, 400
bg = (255,255,255)
fullscreen = False
screen = pygame.display.set_mode(size, RESIZABLE)

pic = pygame.image.load('c://code//pic.png')
position = pic.get_rect()
speed= [1,1]



while True:
	if position.left<0 or position.right > width:
		pic = pygame.transform.flip(pic, True, False)
		speed[0]=-speed[0]
	if position.top<0 or position.bottom>height:
		speed[1]=-speed[1]
	for e in pygame.event.get():
		if e.type == VIDEORESIZE:
			size = e.size
			width,height = size
			pygame.display.set_mode(size, RESIZABLE)
			position = pic.get_rect()
		if e.type == pygame.QUIT:
			sys.exit()
		if e.type ==KEYDOWN:

			if e.key == K_LEFT:
				pic = pygame.transform.flip(pic, True, False)
				speed = [-1,0]
			if e.key == K_RIGHT:
				pic = pygame.transform.flip(pic, True, False)
				speed = [1,0]
			if e.key == K_UP:
				speed = [0,-1]
			if e.key== K_DOWN:
				speed = [0,1]
			# fullscreen
			if e.key ==K_F11:
				fullscreen = not fullscreen
				if fullscreen:
					pygame.display.set_mode((1024,768), FULLSCREEN | HWSURFACE)
					width, height = (1024,768)
				else:
					pygame.display.set_mode(size)
	
		
	position = position.move(speed)
	
	screen.fill(bg)
	screen.blit(pic, position)
	pygame.display.flip()
	clock.tick(100)